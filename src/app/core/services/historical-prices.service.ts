import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {subDays} from "date-fns";
import {HistoryCoinDetails} from "@core/models/coinDetails.model";

@Injectable({
  providedIn: 'root'
})
export class HistoricalPricesService {
  private config = { headers: {
      'Accept': 'application/json',
      'X-CoinAPI-Key': 'D907AEB8-3552-48F3-A68D-5C4295277E94'
    }}

  constructor(private readonly http: HttpClient) {}

  public getHistoryPrices(coinName: string): Observable<HistoryCoinDetails[]> {
    const splitedCoin = coinName.split('/');
    const url =  `https://rest.coinapi.io/v1/exchangerate/${splitedCoin[0]}/${splitedCoin[1]}/history?period_id=1HRS&time_start=${subDays(new Date(), 3).toISOString()}&time_end=${new Date().toISOString()}`;
    return this.http.get<HistoryCoinDetails[]>(url, this.config);
  }
}
