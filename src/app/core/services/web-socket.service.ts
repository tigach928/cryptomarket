import {Injectable} from '@angular/core';
import {Observable, ReplaySubject, Subject} from "rxjs";
import {CoinDetails} from "@core/models/coinDetails.model";

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  public coin = new ReplaySubject<string>(1);
  private ws: WebSocket;
  private subject$ = new Subject<CoinDetails>();

  constructor() {
    this.coin.subscribe((coin) => {
      if (this.ws) this.ws.close();
      this.connect(coin.toUpperCase());
    })
  }

  private connect(coinName: string): void {
    const url = 'wss://ws.coinapi.io/v13/';
    this.ws = new WebSocket(url);

    this.ws.onopen = () => {
      this.ws.send(JSON.stringify({
        "type": "hello",
        "apikey": "D907AEB8-3552-48F3-A68D-5C4295277E94",
        "heartbeat": false,
        "subscribe_data_type": ["trade"],
        "subscribe_filter_asset_id": [coinName]
      }));
    };

    this.ws.onmessage = (event) => {
      this.subject$.next(JSON.parse(event.data));
    };

    this.ws.onerror = (event) => {
      console.error('WebSocket error:', event);
    };
  }

  public getMessages(): Observable<CoinDetails> {
    return this.subject$.asObservable();
  }

}
