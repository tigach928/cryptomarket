import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export function containsSlashValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const forbidden = control.value.includes('/') && control.value.split('/')[1].length;
    return !forbidden ? { 'containsSlash': { value: control.value } } : null;
  };
}
