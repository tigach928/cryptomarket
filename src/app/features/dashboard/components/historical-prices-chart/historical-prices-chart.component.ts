import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {HistoricalPricesService} from "@core/services/historical-prices.service";
import {ECharts} from "echarts";
import {format} from "date-fns";
import {Subject, takeUntil} from "rxjs";
import {HistoryCoinDetails} from "@core/models/coinDetails.model";

@Component({
  selector: 'app-historical-prices-chart',
  templateUrl: './historical-prices-chart.component.html',
  styleUrls: ['./historical-prices-chart.component.scss']
})
export class HistoricalPricesChartComponent implements OnInit, OnDestroy {
  @Input() coinName: string;
  public initOptions = { height: '400px' };
  public options: any = {
    xAxis: {
      type: 'category',
      data: []
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        data: [],
        type: 'line',
        smooth: true,
        animation: true,
        tooltip: {
          show: true
        }
      }
    ],
  };
  public chart: ECharts;
  private destroy$ = new Subject<boolean>();

  constructor(private readonly historicalPricesService: HistoricalPricesService) {}

  public ngOnInit(): void {
    this.historicalPricesService
      .getHistoryPrices(this.coinName.toUpperCase())
      .pipe(takeUntil(this.destroy$))
      .subscribe((value: HistoryCoinDetails[]) => {
        console.log(value);
        this.fillChartDate(value);
      })
  }

  public onChartInit(chart: ECharts) {
    if (!this.chart) {
      this.chart = chart;
    }
  }

  private fillChartDate(value: HistoryCoinDetails[]): void {
    const xAxisData: string[] = [];
    const seriesDate: number[] = [];

    value.forEach((value) => {
      xAxisData.push(format(value.time_period_start, 'MM-dd-yyyy'));
      seriesDate.push(value.rate_high);
    })

    this.options.xAxis.data = xAxisData;
    this.options.series[0].data = seriesDate;

    this.chart?.setOption(this.options);
  }

  public ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
