import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {containsSlashValidator} from "@shared/validators/contains-slash-validators";
import {WebSocketService} from "@core/services/web-socket.service";
import {Subject, takeUntil} from "rxjs";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  public coin = new FormControl('', [Validators.required, containsSlashValidator()]);
  public showDetails = false;

  private destroy$ = new Subject<boolean>();

  constructor(private readonly webSocketService: WebSocketService) {}

  public ngOnInit(): void {
    this.coin.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.showDetails = false;
    });
  }

  public showCoinDetails(): void {
    this.webSocketService.coin.next(this.coin.value.split('/')[0]);
    this.showDetails = true;
  }

  public ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
