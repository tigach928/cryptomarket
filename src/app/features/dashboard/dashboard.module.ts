import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {SharedModule} from "../../shared/shared.module";
import { MarketPriceComponent } from './components/market-price/market-price.component';
import { HistoricalPricesChartComponent } from './components/historical-prices-chart/historical-prices-chart.component';
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    DashboardComponent,
    MarketPriceComponent,
    HistoricalPricesChartComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    NgxEchartsModule,
    NgxEchartsModule.forRoot({ echarts }),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DashboardModule { }
