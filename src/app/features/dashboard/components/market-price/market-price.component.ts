import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject, takeUntil, throttleTime} from "rxjs";
import {WebSocketService} from "@core/services/web-socket.service";
import {CoinDetails} from "@core/models/coinDetails.model";

@Component({
  selector: 'app-market-price',
  templateUrl: './market-price.component.html',
  styleUrls: ['./market-price.component.scss']
})
export class MarketPriceComponent implements OnInit, OnDestroy {
  @Input() coinName: string;
  public coinDetails: CoinDetails;

  private destroy$ = new Subject<boolean>();

  constructor(private readonly webSocketService: WebSocketService) {}

  public ngOnInit(): void {
    const stompClient = this.webSocketService.getMessages();
    stompClient.pipe(throttleTime(5000), takeUntil(this.destroy$)).subscribe((coin: CoinDetails) => {
      this.coinDetails = coin;
    })
  }

  public ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
